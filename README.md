# Quality Control Tutorial for Sequencing Data

## Dependencies

- FastQC v0.11.9
- Fastp v0.23.2
- MultiQC v1.13.dev0

## Workflow Overview

The following workflow describes the adapter removal and quality trimming process for raw short read sequencing dataset. The adapter removal and read quality trimming is done using Fastp[1] version 0.23.2, and the quality assessment and report generation is done using FastQC[2] version 0.11.9 and MultiQC[3] version 1.13.dev0.    

![Workflow Overview](/images/QC_Tutorial.drawio.png)

## Raw Input Data

The sequencing data we are using in this tutorial is a metagenomic shotgun sequencing dataset of [ZymoBIOMICS Microbial Community Standard II (Log Distribution)](https://www.zymoresearch.com/collections/zymobiomics-microbial-community-standards/products/zymobiomics-microbial-community-standard-ii-log-distribution). The sample is a mock microbial community consisting of eight bacterial (three gram-negative and five gram-positive) and two yeast species. The theoretical abundances of the species in the dataset follows a log-distribution. The dataset is generated using shotgun sequencing on an Illumina MiSeq platform, which generates paired-end reads with read length of 150bp. The sequencing data comes from a previous study[4], and we thank the authors of the study to make the data publically available.

The sequencing dataset can be downloaded from NCBI SRA database with SRA accession number: [SRR18488973](https://www.ncbi.nlm.nih.gov/sra/?term=SRR18488973)

We will first have to start by activating the conda environment containing the three dependencies above, and then downloading and extracting the raw sequence data from a convenient source:
```
conda activate treangen
wget https://www.dropbox.com/s/0w0wp73mw7i1t0l/quality_control_short_read_zymo.tar.gz
tar -xzf quality_control_short_read_zymo.tar.gz
```

The data is stored under directory ```raw_sequences```, and you may check those file using the following command.
```
ls raw_sequences/
```

The sequencing data is stored in fastq format. Running the following command would give you an idea of what the format looks like.
```
head raw_sequences/SRR18488973_1.fastq
```
You should be able to see the following:
```
@SRR18488973.1 1 length=150
CCGCTTTTGAGGCCATTTCAGAACTTGAACTAACGAGACCAGCTGCTCCCATATCTTGAATTCCCACTAAAATATCCGAATGATCACGAATCACATCTAAACAAGCTTCAAGCAACAATTTCTCCATAAAAGGATCGCCAACCTGTACAG
+SRR18488973.1 1 length=150
>11>AAAF1111AA1BGGGBGFHADG111BFHHHGGG0AA00/FGCFGHGHG0DEFG2AFHHHHG1BF0111FHHHHGG?FF1FE2FA//FE@F@GGHFB1FBCBEHHHHGHHHBFA/GGHHHHHHHG1111/0BGAEEA/?AGEGBD22
...
```

Each read in the fastq format consist of 4 lines. 
- Line 1 begins with a '@' character and is followed by a sequence identifier and an optional description.
- Line 2 store the nucletides in `A`, `T`, `C`, and `G`s. Ambiguous bases are stored as `N`s. 
- Line 3 begins with a '+' character and is optionally followed by the same sequence identifier (and any description) again.
- Line 4 encodes the quality values for the sequence in Line 2, and must contain the same number of symbols as letters in the sequence. 

The quality score is line 4 is also known as phred quality score. It indicates base-calling error probability P of each bases. In fastq format, those scores (Q ranging from 1-60) are encoded as ASCII characters. It is defined as Q=-10*log10(P). The higher the score, the more accurate the base call is. For example, Q=10 indicates the bases has a error probablity of 10%, Q=20 indicates the bases has a error probablity of 1%, Q=30 indicates the bases has a error probablity of 0.1%. The quality score is one of the most important evidence of data quality, and read quality trimming algorithm relies on those scores to determine whether to trim off the bases, or even to discard the entire read. 

## Pre-filtering Data Quality Assessment

Let's started by preforming data quality assessment using FastQC on the raw sequences. Execute the following command:
```
fastqc raw_sequences/SRR18488973_1.fastq raw_sequences/SRR18488973_2.fastq
```
FastQC would generate one report (html) for each of the input files in `raw_sequences/` directory. You can viev the report in a web brower. Let's try open `SRR18488973_1_fastqc.html`, and you should see the following report.

![Pre-filtering Data Quality](/images/pre-filtering.png)

There are some basic statistics shown on the top of the page, such as read length, total number of reads, mean of the GC context, etc. `Per base sequence quality` shows the sequencing quality of each bases on the reads. We noticed that most of the reads has an overall good quality (above 30, meaning the base calling accuracy is above 99.9%). The ends of the reads (both front and back) usually contains bases with lower quality, as you can see from the figure, which is related to the mechanism of the sequencing technology and such quality decrease at the ends of the sequence is totally normal. 

Although the overall quality for the dataset looks good, it does not guarantee good quality for each individual reads. Quality trimming in the next step allows us to cut and discard the low quality ends, which we will discuss later. 

![Pre-filtering Adapters](/images/pre-filtering-adapters.png)

At the end of the report, we noticed the dataset still contains some adapter sequences. Adapter sequences are oligonucleotide sequences introduced during the sample preperation process, and they are not originated from the microbiome community. When read length exceeds DNA insert size, a run can sequence beyond the DNA insert and read bases from the sequencing adapter. Trimming the adapter sequence improves the accuracy and performance in the downstream analysis. In the next step, we could remove those adapter sequences and do quality trimming of the reads at the same time.

## Adapter Removal and Quality Trimming

Next, we are going to remove adapter sequences and perform quality trimming using fastp with the following command:

```
mkdir filtered_sequences
fastp --in1 raw_sequences/SRR18488973_1.fastq --in2 raw_sequences/SRR18488973_2.fastq --out1 filtered_sequences/SRR18488973_1.filtered.fastq --out2 filtered_sequences/SRR18488973_2.filtered.fastq --cut_front --cut_tail --cut_window_size 4 --cut_mean_quality 30 --qualified_quality_phred 30 --unqualified_percent_limit 30 --n_base_limit 5 --length_required 80 --detect_adapter_for_pe
```

The parameter we used for quality trimming are the following, and fastp will run a sliding window of size 4 bp from both end, check whether the mean quality of the bases in the sliding window, and cut the sequences if this mean quality is less than 30. The trimming stops once the mean quality of the bases in the sliding window excess 30. In other words, this algorithm only trims the low quality bases at the ends of the reads.

```
    --cut_front                          move a sliding window from front (5') to tail, drop the bases in the window if its mean quality < threshold, stop otherwise.
    --cut_tail                           move a sliding window from tail (3') to front, drop the bases in the window if its mean quality < threshold, stop otherwise.
    --cut_window_size 4                  the window size option shared by cut_front, cut_tail or cut_sliding. Range: 1~1000
    --cut_mean_quality 30                the mean quality requirement option shared by cut_front, cut_tail or cut_sliding. Range: 1~36
```

By default, fastp enables adapter removal, and adding `--detect_adapter_for_pe` parameter allows it to automatically detect adapters for paired-end(PE) data. The user can also provide their own adapter sequences as a reference. 

We also filters the reads based on its overall quality and length after trimming with the following parameters. So if a read pair contains more than 30% of the bases with quality lower than 30, or more than 5 ambiguous bases, the entire read pair is discard. The read with length less than 80 (after trimming and adapter removal) is also discard.

```
    --qualified_quality_phred 30     the quality value that a base is qualified. Default 15 means phred quality >=Q15 is qualified.
    --unqualified_percent_limit 30   how many percents of bases are allowed to be unqualified (0~100).
    --n_base_limit 5                 if one read's number of N base is >n_base_limit, then this read/pair is discarded.
    --length_required 80             reads shorter than length_required will be discarded.
```

Fastp is a tool designed to provide fast all-in-one preprocessing for FastQ files, and there are a lot more quality control features and advance filtering options, such as low complexity filtering, base correction, polyG/polyX trimming, etc. In this tutorial, we are only covering some of the basic filters. For a detailed description of the tool, please visit [Fastp github repo](https://github.com/OpenGene/fastp).

Fastp should output the following statistics about the adapter removal and read quality trimming before and after the process. The report shows the adapter sequences found in the data, read/base count and their mean qualities before and after filtering, and how many reads are discard due to different reasons. 

```
Detecting adapter sequence for read1...
>Illumina TruSeq Adapter Read 1
AGATCGGAAGAGCACACGTCTGAACTCCAGTCA

Detecting adapter sequence for read2...
>Illumina TruSeq Adapter Read 2
AGATCGGAAGAGCGTCGTGTAGGGAAAGAGTGT

Read1 before filtering:
total reads: 817619
total bases: 122642850
Q20 bases: 121369217(98.9615%)
Q30 bases: 120977754(98.6423%)

Read2 before filtering:
total reads: 817619
total bases: 122642850
Q20 bases: 120410587(98.1799%)
Q30 bases: 119808817(97.6892%)

Read1 after filtering:
total reads: 798589
total bases: 116658494
Q20 bases: 115960291(99.4015%)
Q30 bases: 115731407(99.2053%)

Read2 after filtering:
total reads: 798589
total bases: 116466764
Q20 bases: 115528386(99.1943%)
Q30 bases: 115238080(98.945%)

Filtering result:
reads passed filter: 1597178
reads failed due to low quality: 31426
reads failed due to too many N: 0
reads failed due to too short: 6634
reads with adapter trimmed: 286362
bases trimmed due to adapters: 5954391

Duplication rate: 0.0872044%

Insert size peak (evaluated by paired-end reads): 185
```

## Post-filtering Data Quality Assessment

Now let's preform data quality assessment using FastQC on the the filtered sequences. Execute the following command:

```
fastqc filtered_sequences/SRR18488973_1.filtered.fastq filtered_sequences/SRR18488973_2.filtered.fastq
```

If you compare the reports before and after quality trimming and adapter removal, you can see the qualities of the bases near the ends have gone up in `Per base sequence quality` figure, and in `Per sequence quality scores`, we no longer find sequences with mean quality below 28. FastQC reports also show that adapter sequences has been successfully removed from the dataset.

![Post-filtering Quality](/images/before_vs_after_qc.png)

![Post-filtering Adapters](/images/post-filtering-adapters.png)

## Generate Comprehensive Report

You may also generate a comprehensive report using MultiQC with the following command:

```
multiqc .
```

MultiQC searches a given directory for analysis logs and compiles a HTML report. It's a general use tool, perfect for summarising the output from numerous bioinformatics tools. If you wish to learn more about MultiQC, please visit [https://multiqc.info/](https://multiqc.info/). MultiQC supports more than 110 tools which are listed [here](https://multiqc.info/#supported-tools). In this tutorial, we use MultiQC to combine FastQC and Fastp reports.  

![MultiQC Report](/images/multiqc.png)

## Alternative Trimming Tools

There are many other tools design to preprocess short sequencing reads. Two of the well-known alternative solutions are:

- trimmomatic[5]
- cutadapt[6]

Both trimmomatic and cutadapt provides similar features in terms of technical sequence removal and trimming, with some differences in adapter searching algorithm. Trimmomatic has a palindrome mode that optimized for an edge case called ‘adapter read-through’, where reads contain adapter sequences from the other opposite side. cutadapt supports IUPAC wildcard characters in the adapter sequences. Both tools require user to provide their own adapter sequences as input. 

If you wish to learn more about trimmomatic, please visit [https://github.com/usadellab/Trimmomatic](https://github.com/usadellab/Trimmomatic). 

To learn more about cutadapt, please visit [https://cutadapt.readthedocs.io/en/stable/](https://cutadapt.readthedocs.io/en/stable/).

## Removing Host Contamination

Host contamination removal is often required for raw clinical sequencing data, where the sample contains host(human) DNA. This is usually done by mapping the reads to human reference genome using Bowtie2[7], and then extract the reads/read pairs that are unmapped (both unmapped for PE reads) with samtools[8]. The sequencing data in this tutorial is sequenced from a host-free mock community, and does not require host contamination removal.  

## Citations

[1] Andrews, S. (2010). FastQC:  A Quality Control Tool for High Throughput Sequence Data [Online]. Available online at: [http://www.bioinformatics.babraham.ac.uk/projects/fastqc/](http://www.bioinformatics.babraham.ac.uk/projects/fastqc/)

[2] Chen, Shifu, et al. "fastp: an ultra-fast all-in-one FASTQ preprocessor." Bioinformatics 34.17 (2018): i884-i890.

[3] Ewels, Philip, et al. "MultiQC: summarize analysis results for multiple tools and samples in a single report." Bioinformatics 32.19 (2016): 3047-3048.

[4] Hempel, Christopher A., et al. "Metagenomics vs. total RNA sequencing: most accurate data-processing tools, microbial identification accuracy, and implications for freshwater assessments." bioRxiv (2022).

[5] Bolger, Anthony M., Marc Lohse, and Bjoern Usadel. "Trimmomatic: a flexible trimmer for Illumina sequence data." Bioinformatics 30.15 (2014): 2114-2120.

[6] Martin, Marcel. "Cutadapt removes adapter sequences from high-throughput sequencing reads." EMBnet. journal 17.1 (2011): 10-12.

[7] Langmead, Ben, and Steven L. Salzberg. "Fast gapped-read alignment with Bowtie 2." Nature methods 9.4 (2012): 357-359.

[8] Li, Heng, et al. "The sequence alignment/map format and SAMtools." Bioinformatics 25.16 (2009): 2078-2079.
